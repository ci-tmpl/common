# Build and deploy backend e2e test or review apps ---------------------------------------------------------------------
#
# Variables:
# * APP_ENVIRONMENT
# * APP_BUILD_PATH
# * APP_PATH                  – # folder for e2e test: API / Instance / Main APP
# * APP_DB                    – # DB: name, user and password for e2e test: API / Instance / Main APP | cut used because of MySQL username limit.
# * APP_URL                   – # link for e2e API test Main APP
# * APP_FE_URL
#
.e2e/ra-backend-deploy:
  script:
    # ---
    - echo $APP_BUILD_PATH
    - echo $APP_PATH
    - echo $APP_DB
    - echo $APP_DB_INSTANCE
    - echo $APP_URL
    - echo $APP_ENVIRONMENT
    - echo $APP_FE_URL
    - echo $CI_DB_DATABASE
    - echo $CI_DB_USERNAME
    - echo $CI_DB_PASSWORD
    - echo $CI_JWT_SECRET
    - echo $IS_JWT
    # ---
    - if [[ -d "$APP_PATH" ]]; then rm -Rf $APP_PATH; fi                                        # remove temp e2e folder if exist
    - cp -rd $APP_BUILD_PATH $APP_PATH                                                          # copy project files to temp e2e folder (-d = with symbolic links)
    - cd $APP_PATH                                                                              # enter to temp e2e folder
    # install all project dependencies (with dev dependencies)
    - composer install --no-ansi --no-interaction --no-plugins --no-scripts --no-progress --optimize-autoloader # optimize-autoloader: needed here coz we add dev dependencies here.
    # create temporary DB by admin credentials 1
    - cp .env.example .env                                                                      # create fresh .env file
    - sed -i "s|{{DB_DATABASE}}||g" .env
    - sed -i "s|{{DB_USERNAME}}|$CI_ADMIN_DB_USERNAME|g" .env
    - sed -i "s|{{DB_PASSWORD}}|$CI_ADMIN_DB_PASSWORD|g" .env
    - php artisan config:cache
    - APP_DB=$(echo $APP_DB | cut -c -32)                                                       # cut APP_DB string to 32 characters
    - php artisan db:create $APP_DB $APP_DB $APP_DB                                             # create temp e2e DB and user
    # refresh cache and fill DB
    - cp .env.example .env                                                                      # create fresh .env file
    - php artisan down
    - php artisan key:generate
    - sed -i "s|{{DB_DATABASE}}|$APP_DB|g" .env
    - sed -i "s|{{DB_USERNAME}}|$APP_DB|g" .env
    - sed -i "s|{{DB_PASSWORD}}|$APP_DB|g" .env
    - sed -i "s|{{APP_URL}}|$APP_URL|g" .env
    - sed -i "s|{{APP_ENV}}|$APP_ENVIRONMENT|g" .env
    - sed -i "s|{{FRONTEND_URL}}|$APP_FE_URL|g" .env
    - sed -i "s|{{MEILISEARCH_KEY}}|$CI_MEILISEARCH_KEY|g" .env
    - if [[ $IS_JWT && $CI_JWT_SECRET ]]; then sed -i "s|{{JWT_SECRET}}|$CI_JWT_SECRET|g" .env; fi
    - if [[ $IS_JWT && ! $CI_JWT_SECRET ]]; then sed -i "s|{{JWT_SECRET}}|$(php artisan jwt:secret --force --show)|g" .env; fi
    # Crm integration variables (skips if not exist)
    - sed -i "s|{{CRM_CLIENT_ID}}|$CI_CRM_CLIENT_ID|g" .env
    - sed -i "s|{{CRM_CLIENT_SECRET}}|$CI_CRM_CLIENT_SECRET|g" .env
    - sed -i "s|{{CRM_PORTAL_DOMAIN}}|$CI_CRM_PORTAL_DOMAIN|g" .env
    # Service App variables only! (skips if not exist)
    - sed -i "s|{{GITLAB_SPACE}}|$GITLAB_SPACE|g" .env
    - sed -i "s|{{GITLAB_PERSONAL_TOKEN}}|$CI_GITLAB_PERSONAL_TOKEN|g" .env
    - sed -i "s|{{APPS_DOMAIN}}|$DOMAIN_DEV|g" .env
    # set project related environment variables (LOCAL fragment)
    - !reference [ .backend-custom-env-set, script ]
    - > # create instance db and set db connection in tenant seeder
      if [[ $IS_SAAS ]]; then
        APP_DB_INSTANCE=$(echo $APP_DB_INSTANCE | cut -c -32);
        php artisan db:create $APP_DB_INSTANCE $APP_DB_INSTANCE $APP_DB_INSTANCE;     
        sed -i "s|'tenancy_db_name' => 'instance'|'tenancy_db_name' => '$APP_DB_INSTANCE'|g" $APP_PATH/database/seeders/e2e/TenantSeeder.php;
        sed -i "s|'tenancy_db_password' => 'instance'|'tenancy_db_password' => '$APP_DB_INSTANCE'|g" $APP_PATH/database/seeders/e2e/TenantSeeder.php;
        sed -i "s|'tenancy_db_username' => 'instance'|'tenancy_db_username' => '$APP_DB_INSTANCE'|g" $APP_PATH/database/seeders/e2e/TenantSeeder.php;
      fi
    # print resulting env file
    - echo "$(<.env)"
    - php artisan config:cache
    - php artisan migrate:fresh --seed
    - if [[ $IS_SAAS ]]; then php artisan test:set-tenant instance; fi
    - php artisan cache:clear
    - php artisan route:cache
    - if [[ -d "resources/views" ]]; then php artisan view:cache; fi
    - php artisan up

# Clear temp e2e test or review app folders with DBs -------------------------------------------------------------------
#
# Variables:
# * APP_PATH
# * APP_DB
#
.e2e/ra-backend-clear:
  script:
    # ---
    - echo $APP_PATH
    - echo $APP_DB
    - echo $APP_DB_INSTANCE
    # ---
    - >
      if [[ -d "$APP_PATH" ]]; then
        echo "Deleting project..."
        cd $APP_PATH;
        cp .env.example .env;
        sed -i "s|{{DB_DATABASE}}||g" .env
        sed -i "s|{{DB_USERNAME}}|$CI_ADMIN_DB_USERNAME|g" .env;
        sed -i "s|{{DB_PASSWORD}}|$CI_ADMIN_DB_PASSWORD|g" .env;
        php artisan config:cache;
        APP_DB=$(echo $APP_DB | cut -c -32);
        php artisan db:delete $APP_DB;
        APP_DB_INSTANCE=$(echo $APP_DB_INSTANCE | cut -c -32);
        php artisan db:delete $APP_DB_INSTANCE;
        rm -Rf $APP_PATH;
      fi

# Prepare slugs (to fits max allowed subdomain length = 63 characters) -------------------------------------------------
#
.ra-backend-slugs:
  script:
    - FE_RA_SLUG=$(echo ra-$SUB_DOMAIN_DEV-$CI_COMMIT_REF_SLUG | cut -c -63)

.ra-backend-api-slugs:
  script:
    - API_RA_SLUG=$(echo ra-$API_SUB_DOMAIN_DEV-$CI_COMMIT_REF_SLUG | cut -c -63)

#
# Local fragments (copy it from here to projects .gitlab-ci.yml file)
# Sets environment variables by replacing template strings "{{...}}" in .env file --------------------------------------
#
.backend-custom-env-set:
  script:
    - > # Set variables only for DEVELOPMENT environment
      if [[ $CI_ENVIRONMENT_SLUG == 'development' ]]; then
        sed -i "s|{{VARIABLE}}|$VARIABLE|g" .env;
      fi
    - > # Set variables only for PRODUCTION environment
      if [[ $CI_ENVIRONMENT_SLUG == 'production' ]]; then
        sed -i "s|{{VARIABLE}}|$VARIABLE|g" .env;
      fi
    # Set variables for ALL environments or for environment related CI/CD variables
    - sed -i "s|{{VARIABLE}}|$VARIABLE|g" .env
